provider "aws" {
  region     = "us-east-1"
}

resource "aws_instance" "nhernandez" {
  ami           = "ami-0cc96feef8c6bbff3"
  instance_type = "t2.micro"

  tags = {
    Name = "test-nhernandez"
  }
}
