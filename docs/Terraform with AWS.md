## Terraform with AWS

#### Creation the VPC

* On AWS you have a default VPC
* One Vpc in to subnets, and subnets are then in particularly AZ and subnets can be public or private 
* All the public subnets are connected to an Internet Gateway, these instances will also have a public IP address, allowing them to be reachable from the internet.
* Instances launched in the Private subnets don't get a public IP address, so they will not be reachable from the internet
* Instances from main-public can reach from main-private, because they're all in the same VPC
* Typically, you use the public subnets for internet-facing services/apps
* Databases, caching services, and brackens all go into the private subnets
* If you use a LB, you will typically put the LB in the public subnets and the instances serving an application in the private subnets



_____________

#### EC2 Instances in the VPCs

