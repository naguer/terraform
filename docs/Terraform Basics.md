## Terraform Basics

#### Variables

- Use variables for elements that might change
- Use variables to hide secrets
- Use variables to make it yourself easy to reuse
  

______

#### Files

- Provider.tf: Contain the provider, the secret, and the region, refeer to a variables
- vars.tf: Declare the variables and default variables, region, ami ids, etc
- terraform.tfvars: put values of variables, this is going to .gitignore because its have secrets
- instance.tf: The resources
  

____

#### Remote State (tfstate)

* Terraform keeps the remote state of the infrastructure in a file called terraform.tfstate
* There is also a backup of the previous state in terraform.tfstate.backup
* When you execute terraform apply, a new .tfstate and backup is written
* This is how terraform keeps track of the remote state
* The tfstate can be saved remote, using the backend functionality in terraform
* The default is a local backend (the local terraform state file)
* Another backend is s3 with a locking mechanism using dynamoDB
* Terraform doesn't recreate the terraform tfstate file itself automatically. You can use terraform import to manually recreate the terraform.tfstate.
* Using a remote store for the terraform state will ensure that you always have the lastest version of the state

Example (after of create the file do terraform init)

```
terraform {
        backend "s3" {
                bucket = "terraform-onboarding-nhernandez"
                key = "terraform/terraform.tfstate"
        }
}
```



______________

#### Datasources

* Datasources provide you with dynamic information

  * a lot of data is available by AWS in a structured format using their API
  * Terraform also exposes this information using data sources

* If you need dynamic information, you can use datasources instead of variables. Datasources will pull their information from a remote API before it turns into a variable that can be used in *.tf files

* Examples: List of AMIs, List of AZ (Dynamic information)

* Another great example is the datasource that gives you all IPs in use by AWS, this is great if you want to filter traffic bases on an AWS region (eg allow al traffic from Europe)

  eg:

```
data "aws_ip_ranges" "european_ec2" {
  regions  = ["eu-west-1", "eu-central-1"]
  services = ["ec2"]
}
```

_______

#### Template provider

* The template provider can help creating customized configuration files
* You can build templates based on variables from terraform resource attributes (e.g. a public IP)
* The result is a string that can be used as a variable in terraform
  * The string contains a template
  * e.g. a config file
* In AWS, you can pass commands that need to be executed when the instance starts for the first time, this is called "user-data"
* Is for extract information and process with another program



____________

#### Modules

* You can use modules to make your terraform more organized
* Use third party modules
  * Like github
* Reuse parts of your code
  * e.g. to set up network in AWS 
* You can use modules locally or remotely like github
* Before use a module we need to execute a "terraform get" to download the module



________

#### Commands

| Command  | Description                                                  |
| :------: | ------------------------------------------------------------ |
|   show   | more readable format, utile with grep to get what you need   |
|  taint   | Manually mark a resource as tainted, forcing a destroy and recreate on the next plan/apply |
| untaint  | remove the "taint" mark                                      |
|  graph   | To visual dependency graph, you can use with Graphviz or Blast-radius |
| refresh  | Attempts to find any resources held in the state file and update with any drift that has happened in the provider outside of Terraform since it was last ran |
|   fmt    | Rewrite Terraform configuration files to the same style. For default fmt scan the current directory for config files |
| validate | To check the syntax of one file                              |
|  import  | Import existing infrastructure. This allows you take resources you've created by some other means and bring it under Terraform management. It does not generate configuration |



_____









